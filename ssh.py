# invite link https://discord.com/oauth2/authorize?client_id=737538862615625859&scope=bot&permissions=8

import discord
from discord.ext import commands
import sys

TOKEN = ""
users_list=[]
command_given = False
announced_channel = ''
DESC = "Control your linux pc using this bot."
PRE = 's' # default prefix

bot = commands.Bot(command_prefix=PRE, description = DESC, activity=discord.Game(name='SSH Terminal'))

async def log(*args):
    #print('sending log') https://unix.stackexchange.com/questions/3886/difference-between-nohup-disown-and#148698
    msg = 'LOG: '
    for arg in args:
        msg += str(arg) + ' '
    await bot.get_channel(LOG).send(msg)

def nick_name(auth):
    if isinstance(auth, discord.User):
        return auth.name
    if isinstance(auth.nick, str):
        return auth.nick
    return auth.name

@bot.command()
async def status(ctx): # change the status of the bot
    """Change the status of the bot (tatus playing SSH or tatus streaming My song *url*)"""
    global command_given
    command_given = True

    await log("status change by", ctx.message.author, "content", ctx.message.content)

    msg = ctx.message.content.split()
    msg.pop(0)

    if len(msg) == 0:
        return

    playing = ['playing', 'play', 'g', 'game']
    steaming = ['streaming', 's', 'stream']
    listening = ['listening', 'l', 'listen']
    watching = ['watching', 'w', 'watch']
    # Setting `Playing ` status
    if msg[0] in playing:
        msg.pop(0)
        await bot.change_presence(activity=discord.Game(name=' '.join(msg)))

    # Setting `Streaming ` status
    elif msg[0] in steaming:
        msg.pop(0)
        await bot.change_presence(activity=discord.Streaming(name=' '.join(msg[:-1]), url=msg[-1]))

    # Setting `Listening ` status
    elif msg[0] in listening:
        msg.pop(0)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name=' '.join(msg)))

    # Setting `Watching ` status
    elif msg[0] in watching:
        msg.pop(0)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=' '.join(msg)))

    else: # defaulting to playing
        await bot.change_presence(activity=discord.Game(name=' '.join(msg)))

@bot.command()
async def reboot(ctx):
    global command_given
    command_given = True
    await log("exiting! command by",ctx.author, 'reason', ctx.message.content)
    sys.exit(0)

# command window
c_window


@bot.event
async def on_message(message):
    await bot.process_commands(message)
    global command_given

    if command_given:
        command_given = False
        return

f = open("token", "r")
TOKEN = f.read()
TOKEN = TOKEN[:-1]

bot.run(TOKEN)
